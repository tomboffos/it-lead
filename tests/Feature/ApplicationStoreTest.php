<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApplicationStoreTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/applications',[
            'name' => 'Асыл',
            'phone' => '+7 (707) 303-99-17',

        ]);

        $response->json();
        $response->assertStatus(200);
    }
}
