<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'name' => $this->name,
            'idea' => $this->idea,
            'work' => $this->work,
            'realization' => $this->realization,
            'logo' => asset('storage/'.$this->logo),
            'images' => ImageTrait::decodeImageArray($this->files),
            'link' => $this->link,
            'web' => $this->web
        ];
    }
}
