<?php


namespace App\Http\Resources;


class ImageTrait
{

    public static function decodeImageArray($images)
    {
        $imageArray = [];

        $images = json_decode($images);

        foreach ($images as $image)
            $imageArray[] = asset('storage/' . $image);

        return $imageArray;
    }
}
