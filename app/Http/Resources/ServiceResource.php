<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'short_description' => $this->short_description,
            'description' => $this->description,
            'work' => $this->work,
            'additional' => $this->additional_info,
            'images' => ImageTrait::decodeImageArray($this->images),
            'image' => asset("storage/$this->image"),
            'price' => $this->price,
            'slug' => $this->slug
        ];
    }
}
