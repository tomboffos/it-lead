<?php

namespace App\Http\Controllers\Api\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Interactions\ApplicationRequest;
use App\Models\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    //
    public function store(ApplicationRequest $request)
    {
        $application = Application::create($request->validated());

        return response(['message' => "Спасибо $application->name, ваша заявка будет обработана в течение 2 минут"], 200);
    }
}
