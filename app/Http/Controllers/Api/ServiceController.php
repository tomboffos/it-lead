<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceCategoryResource;
use App\Http\Resources\ServiceResource;
use App\Models\Service;
use App\Models\ServiceCategory;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //
    public function index()
    {
        return ServiceCategoryResource::collection(ServiceCategory::orderBy('order', 'asc')->get());
    }

    public function show(Service $service)
    {
        return new ServiceResource($service);
    }
}
