<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FastSolution;
use Illuminate\Http\Request;

class FastSolutionController extends Controller
{
    //
    public function index()
    {
        return FastSolution::orderBy('order','asc')->get();
    }
}
