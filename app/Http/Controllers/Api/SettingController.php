<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Setting;

class SettingController extends Controller
{
    //
    public function index()
    {
        return $this->transformSettings(Setting::get());
    }


    private function transformSettings($settings){
        $settingsArray = [];
        foreach ($settings as $setting){
            $settingsArray[$setting['key']] = $setting['value'];
        }

        return $settingsArray;
    }
}
