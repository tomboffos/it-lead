<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectCategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class ProjectCategoryController extends Controller
{
    //
    public function index()
    {
        return ProjectCategoryResource::collection(Category::orderBy('order','asc')->get());
    }
}
