<?php

use App\Http\Controllers\Api\About\TechnologyController;
use App\Http\Controllers\Api\FastSolutionController;
use App\Http\Controllers\Api\Interactions\ApplicationController;
use App\Http\Controllers\Api\ProjectCategoryController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\ServiceController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\WorkerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    '/projects' => ProjectController::class,
    '/categories' => ProjectCategoryController::class,
    '/applications' => ApplicationController::class,
    '/technologies' => TechnologyController::class,
    '/settings' => SettingController::class,
    '/services' => ServiceController::class,
    '/workers' => WorkerController::class,
    '/solutions' => FastSolutionController::class
]);
